<?php

namespace App;

/**
 * SQLite Create Table
 */
class SQLiteCreateTable
{

    /**
     * PDO object
     * @var \PDO
     */
    private $pdo;

    /**
     * connect to the SQLite database
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * create tables
     */
    public function createTables()
    {
        $commands = [
            'CREATE TABLE IF NOT EXISTS "users"
        (
            [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            [Email] NVARCHAR(255)  NOT NULL,
            [Password] NVARCHAR(255)  NOT NULL,
            [DateAndTime] NVARCHAR(255)  NOT NULL,
            [TrueOrFalse] NVARCHAR(255)  NOT NULL

        )',
            'CREATE TABLE IF NOT EXISTS "user_roles"
            (
            [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            [UserId] INTEGER  NOT NULL,
            [RoleId] INTEGER  NOT NULL,
            FOREIGN KEY ([UserId]) REFERENCES "users" ([Id])
            ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY ([RoleId]) REFERENCES "roles" ([Id])
            ON DELETE NO ACTION ON UPDATE NO ACTION

            )',
            'CREATE TABLE IF NOT EXISTS "roles"
        (
            [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            [Role] NVARCHAR(255) NOT NULL
        )',
        'CREATE TABLE IF NOT EXISTS "customers"
        (
            [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            [CustomerId] INTEGER  NOT NULL,
            [CompanyName] NVARCHAR(255),
            [FirstName] NVARCHAR(255) NOT NULL,
            [LastName] NVARCHAR(255) NOT NULL,
            [Phone] NVARCHAR(255),
            [Adresse] NVARCHAR(255) ,
            [PostalCode] NVARCHAR(255),
            [City] NVARCHAR(255),
            [Country] NVARCHAR(255),
            FOREIGN KEY ([CustomerId]) REFERENCES "users" ([Id])
                ON DELETE NO ACTION ON UPDATE NO ACTION
        )',
        'CREATE TABLE IF NOT EXISTS "customer_functions"
        (
            [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            [AdresseId] INTEGER  NOT NULL,
            [FunctionId] INTEGER  NOT NULL,
            FOREIGN KEY ([AdresseId]) REFERENCES "customers" ([Id])
                ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY ([FunctionId]) REFERENCES "functions" ([Id])
                ON DELETE NO ACTION ON UPDATE NO ACTION
        )',
        'CREATE TABLE IF NOT EXISTS "functions"
        (
            [Id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            [Function] NVARCHAR(255)
        )'
        ];
        // execute the sql commands to create new tables
        foreach ($commands as $command) {
            $this->pdo->exec($command);
        }
    }
}
