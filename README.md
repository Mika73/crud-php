# CRUD PHP
The application consist in importing an Excel file in a database and to propose a web interface CRUD to display and manage the imported data.
## Requirement
- PHP >= 7.4
- sqlite3
## Setup
1. Download the repository and run
```
composer install
```
2. Import data from an Excel file and create database tables.
```
php import.php
```
3. Launch the php server
```
php -S localhost:3000
```
